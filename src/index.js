import Phaser from "phaser";
import LoadScene from "./scenes/loadScene";
import MenuScene from "./scenes/menuScene";
import PlayScene from "./scenes/playSceneTiled";
import PlaySceneTiled from "./scenes/playScene";

const config = {
	type: Phaser.AUTO,
	parent: "game-container",
	scale: {
		mode: Phaser.Scale.FIT,
		autoCenter: Phaser.Scale.CENTER_BOTH,
		parent: "game-container",
		width: 480,
		height: 320,
		autoResize: true,
	},
	physics: {
		default: "arcade",
		arcade: {
			gravity: { y: 0 },
			fps: 60,
			debug: false,
		},
	},
	pixelArt: true,
	scene: [LoadScene, MenuScene, PlayScene, PlaySceneTiled],
};

new Phaser.Game(config);

