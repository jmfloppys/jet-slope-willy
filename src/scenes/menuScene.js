const options = [
	{ key: "PlayScene", title: "slope from sprite" },
	{ key: "PlaySceneTiled", title: "slope Tiled map" },
];
let optionsArray = [];
let selectOptionIndex = 0;

class MenuScene extends Phaser.Scene {
	constructor() {
		super({ key: "MenuScene" });
	}

	create() {
		const { width, height } = this.game.config;
		const centerX = (textWidth) => width / 2 - textWidth / 2;
    const centerY = (textHeight) => height / 2 - textHeight / 2;
    optionsArray = [];
    selectOptionIndex = 0;

		this.title = this.add.text(0, 0, "Jet Slope Willy", {
			fontFamily: "zx-spectrum",
			fontSize: 16,
			color: "#ffffff",
		});
		this.title.setPosition(
			centerX(this.title.width),
			centerY(this.title.height) / 2
		);
    this.title.setResolution(10);
    
		this.selected = this.add.text(0, 0, "***", {
			fontFamily: "press-start",
			fontSize: 8,
			color: "#ff0000",
		});

		for (let i = 0; i < options.length; i++) {
			let text = this.add.text(0, 0, options[i].title, {
				fontFamily: "press-start",
				fontSize: 8,
				color: "#fff",
			});
			optionsArray.push(text);
		}

		for (let i = 0; i < optionsArray.length; i++) {
			let text = optionsArray[i];
			text.setPosition(centerX(text.width), centerY(text.height) + i * 16);
		}

		this.floor = this.add.sprite(0, this.game.config.height - 32, "floor");
		this.floor.setOrigin(0);

		this.anims.create({
			key: "run",
			frames: this.anims.generateFrameNumbers("player", {
				start: 0,
				end: 3,
				first: 1,
			}),
			frameRate: 6,
			repeat: -1,
		});

		this.player = this.add.sprite(width / 2 - 24, height - 48, "player");
		this.player.anims.play("run");

		this.input.keyboard.on("keydown_DOWN", () => {
			if (selectOptionIndex < optionsArray.length - 1) selectOptionIndex += 1;
		});
		this.input.keyboard.on("keydown_UP", () => {
			if (selectOptionIndex > 0) selectOptionIndex -= 1;
    });
    this.input.keyboard.on("keydown_ENTER", () => {
      this.scene.start(options[selectOptionIndex].key);
    })
	}
	update() {
		const { width } = this.game.config;
		if (this.player.x <= width - this.player.width / 2)
			this.player.flipX ? (this.player.x -= 1) : (this.player.x += 1);

		if (this.getBounds(this.player))
			this.player.flipX
				? this.player.setFlipX(false)
				: this.player.setFlipX(true);

		optionsArray.forEach((e, index) => {
      index === selectOptionIndex ? e.setColor("#ff0000") : e.setColor("#fff")
      if (index === selectOptionIndex) {
        e.setVisible(false);
        this.selected.text = `*** ${e.text} ***`;
        this.selected.setPosition(e.x - 32, e.y);
      } else e.setVisible(true);
    }
		);
	}

	getBounds(sprite) {
		const { width } = this.game.config;
		return (
			sprite.x >= width - sprite.width / 2 || sprite.x <= 0 + sprite.width / 2
		);
	}
}

export default MenuScene;
