const playerVelX = 64;
const playerVelY = -192;
const gravityY = 400;
let map, ground;

class PlaySceneTiled extends Phaser.Scene {
	constructor() {
		super({ key: "PlaySceneTiled" });
	}

	create() {
		this.createGround();
		this.createPlayer();
		this.createText();
		this.createDebug(this.ground);
		this.physics.add.collider(this.ground, this.player);
		this.physics.add.overlap(this.player, this.slopes, (objA, objB) => {
			if (this.intersects(objA.getBounds(), objB.triangle)) {
				if (this.keys.up.isDown) this.jump();
				else {
					let dX;
					let dY = objB.triangle.top + objB.body.height - objA.body.height;
					if (objB.name === "slopeR") {
						dX = objA.body.position.x + objA.body.width - objB.triangle.left;
						objA.body.position.y = dY - dX;
					} else if (objB.name === "slopeL") {
						dX = objA.body.position.x - objB.triangle.right;
						objA.body.position.y = dY + dX;
					}
					objA.body.setAllowGravity(false);
					objA.body.setGravityY(0);
					objA.body.setVelocityY(0);
					objB.touched = true;
					objA.isOnSlope = true;
				}
			} else objA.isOnSlope = false;
		});

		this.input.keyboard.on("keydown_ESC", () => {
			this.scene.start("MenuScene");
		});
	}

	createText() {
		this.text = this.add.text(
			16,
			16,
			["LEFT, RIGHT and UP keys to move", "D to debug", "ESC go back"],
			{ fontFamily: "press-start", fontSize: 8, color: "#ffffff", lineSpacing: 8 }
		);
	}

	createGround() {
		map = this.make.tilemap({ key: "map" });
		const tileset = map.addTilesetImage("tileset", "tileset");
		ground = map.createDynamicLayer("ground", tileset, 0, 0);
		ground.setCollisionByProperty({ collides: true });
		ground.setDepth(0);
		this.ground = ground;

		const spawn = map.findObject("objects", (obj) => obj.name === "player");
		this.spawn = spawn;

		let slopes = [];
		this.slopes = this.physics.add.group();
		map.findObject("slopes", (obj) => {
			slopes = [];
			for (let i = 0; i < obj.polygon.length; i++) {
				let point = obj.polygon[i];
				slopes.push({ x: point.x + obj.x, y: point.y + obj.y });
			}

			const tile = this.physics.add.sprite(0, 0, null).setVisible(false);
			tile.triangle = new Phaser.Geom.Triangle(
				slopes[0].x,
				slopes[0].y,
				slopes[1].x,
				slopes[1].y,
				slopes[2].x,
				slopes[2].y
			);

			let sizeY = tile.triangle.bottom - tile.triangle.top;
			let sizeX = tile.triangle.right - tile.triangle.left;

			tile.body.setEnable(true);
			tile.body.setImmovable(true);
			tile.body.setSize(sizeX, sizeY, true);
			tile.setPosition(obj.x + sizeX / 2, obj.y - sizeY / 2);
			tile.name = obj.name;
			this.slopes.add(tile);
		});

		this.graphics = this.add.graphics({
			lineStyle: { width: 2, color: 0x00aa00 },
		});
	}

	createDebug(tileMapLayer) {
		const graphics = this.add.graphics().setAlpha(0.75).setDepth(20);
		this.input.keyboard.on("keydown_D", () => {
			if (!this.physics.world.drawDebug)
				this.physics.world.createDebugGraphic();
			this.physics.world.debugGraphic.visible = this.debug = !this.debug;
			this.renderDebug(tileMapLayer, graphics);
		});
	}

	renderDebug(tileMapLayer, graphics) {
		graphics.clear();
		if (this.debug) {
			tileMapLayer.renderDebug(graphics, {
				tileColor: null,
				collidingTileColor: new Phaser.Display.Color(243, 134, 48, 200),
				faceColor: new Phaser.Display.Color(40, 39, 37, 255),
			});
		}
	}

	createPlayer() {
		this.anims.create({
			key: "run",
			frames: this.anims.generateFrameNumbers("player", {
				start: 0,
				end: 3,
				first: 1,
			}),
			frameRate: 6,
			repeat: -1,
		});
		this.anims.create({
			key: "idle",
			frames: this.anims.generateFrameNumbers("player", {
				start: 0,
				end: 0,
			}),
		});
		this.anims.create({
			key: "jump",
			frames: this.anims.generateFrameNumbers("player", {
				start: 3,
				end: 3,
			}),
		});

		this.player = this.physics.add.sprite(this.spawn.x, this.spawn.y, "player");
		this.player.name = "player";
		this.player.anims.play("idle");
		this.player.body.gravity.y = gravityY;
		this.player.body.setSize(16, 32, true);
		this.player.setCollideWorldBounds(true);

		const { RIGHT, LEFT, UP } = Phaser.Input.Keyboard.KeyCodes;
		this.keys = this.input.keyboard.addKeys({
			left: LEFT,
			right: RIGHT,
			up: UP,
		});
	}

	jump() {
		this.player.body.setAllowGravity(true);
		this.player.body.setGravityY(gravityY);
		this.player.body.setVelocityY(playerVelY);
	}

	update() {
		const { right, left, up } = this.keys;
		const isGround = this.player.body.blocked.down;

		if ((right.isDown || left.isDown) && (isGround || this.player.isOnSlope))
			this.player.anims.play("run", true);
		else if (!isGround && !this.player.isOnSlope)
			this.player.anims.play("jump", true);
		else this.player.anims.play("idle", true);
		if (right.isDown) {
			this.player.body.setVelocityX(playerVelX);
			this.player.setFlipX(false);
		} else if (left.isDown) {
			this.player.body.setVelocityX(-playerVelX);
			this.player.setFlipX(true);
		} else {
			if (!isGround && !this.player.isOnSlope)
				this.player.body.velocity.x *= 0.98;
			else this.player.body.setVelocityX(0);
		}
		if (isGround && up.isDown) this.player.body.setVelocityY(playerVelY);

		this.graphics.clear();

		this.player.body.setGravityY(gravityY);
		this.player.body.setAllowGravity(true);
		this.slopes.getChildren().forEach((slope) => {
			if (this.physics.world.intersects(this.player.body, slope.body)) {
				if (this.intersects(this.player.getBounds(), slope.triangle))
					this.graphics.lineStyle(2, 0xff0000);
			} else {
				this.graphics.lineStyle(2, 0x282725);
				if (slope.touched) {
					slope.touched = false;
					this.player.isOnSlope = false;
				}
			}
			if (this.debug) {
				this.graphics.fillTriangleShape(slope.triangle);
				this.graphics.strokeTriangleShape(slope.triangle);
			}
		});
	}

	intersects(objA, objB) {
		return Phaser.Geom.Intersects.RectangleToTriangle(objA, objB);
	}
}

export default PlaySceneTiled;
