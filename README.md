# Jet Slope Willy Test

A small pet project created with Phaser 3 just to see if I could handle the slope solution using Arcade Physics given by [https://www.emanueleferonato.com](https://www.emanueleferonato.com/2016/05/04/my-take-on-handling-slopes-with-phaser-and-arcade-physics/) for Phaser 2.

Base template is provided by [Phaser3](https://github.com/photonstorm/phaser3-project-template)


## Run

| Command | Description |
|---------|-------------|
| `npm install` | Install project dependencies |
| `npm start` | Build project and open web server running project |
| `npm run build` | Builds code bundle with production settings (minification, uglification, etc..) |
